#ifndef BICYCLE_HH
#define BICYCLE_HH

#include "interfaces/ivehicle.hh"


class Bicycle : public Interface::IVehicle
{
public:
    Bicycle(Interface::Location loc);

    Interface::Location giveLocation() const override;
    void move(Interface::Location loc) override;
    bool isRemoved() const override;
    void remove() override;

    std::string getName() const override;
    std::vector<std::shared_ptr<Interface::IPassenger> > getPassengers() const override;
    void addPassenger(std::shared_ptr<Interface::IPassenger> passenger) override;
    void removePassenger(std::shared_ptr<Interface::IPassenger> passenger) override;

    bool isFree();

private:
    std::shared_ptr<Interface::IPassenger> passenger_;
    Interface::Location loc_;
    bool removed_;
};

#endif // BICYCLE_HH
