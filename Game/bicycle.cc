#include "bicycle.hh"

Bicycle::Bicycle(Interface::Location loc) :
    passenger_(),
    loc_(loc),
    removed_(false)
{

}

Interface::Location Bicycle::giveLocation() const
{
    return loc_;
}

void Bicycle::move(Interface::Location loc)
{
    loc_ = loc;
}

bool Bicycle::isRemoved() const
{
    return removed_;
}

void Bicycle::remove()
{
    removed_ = true;
}

std::string Bicycle::getName() const
{
    return "bike";
}

std::vector<std::shared_ptr<Interface::IPassenger> > Bicycle::getPassengers() const
{
    return {passenger_};
}

void Bicycle::addPassenger(std::shared_ptr<Interface::IPassenger> passenger)
{
    if (passenger_ == nullptr) {
    passenger_ = passenger;
    }
}

void Bicycle::removePassenger(std::shared_ptr<Interface::IPassenger> passenger)
{
    if (passenger == passenger_) {
        passenger_ = nullptr;
    }
}

bool Bicycle::isFree()
{
    if (getPassengers().at(0)  == nullptr) {
        return true;
    }
    return false;
}
