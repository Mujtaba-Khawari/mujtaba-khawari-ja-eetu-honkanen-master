#include "engine.hh"
#include <stdio.h>
#include <stdlib.h>

Engine::Engine(MainWindow* mainWindow, CourseSide::Logic* logic, QObject* parent) :
    QObject(parent),
    city_(std::static_pointer_cast<City>(Interface::createGame())),
    mainWindow_(mainWindow),
    logic_(logic),
    start_window_(new Dialog),
    numberOfPlayers_(0)
{
    city_->setMainWindow(mainWindow_);
    city_->setStatistics();
    logic_->takeCity(city_);
    logic_->fileConfig(CourseSide::DEFAULT_STOPS_FILE,CourseSide::DEFAULT_BUSES_FILE);
    QImage background(":/img/img/map1.png");
    QImage bigbackground(":/offlinedata/offlinedata/kartta_iso_1095x592.png");
    city_->setBackground(background, bigbackground);
    connect(mainWindow_, &MainWindow::playerMoved, this, &Engine::movePlayer);
    connect(mainWindow_, &MainWindow::playerToStop, this, &Engine::playerInteract);
    connect(mainWindow_, &MainWindow::gameStarted, this, &Engine::gameStarted);
    connect(start_window_,&Dialog::on_oneplayerbutton_clicked,this,&Engine::star_with_one_player);
    connect(start_window_,&Dialog::on_twoplayerbutton_clicked,this,&Engine::start_with_two_player);
    start_window_->show();
    start_window_->setWindowTitle("NYSSE GAME");



}

void Engine::movePlayer(std::shared_ptr<Player> player)
{
    if (!city_->isGameOver() && (!player->isInVehicle() || player->isOnBike())) {
        int speed = 1;

        bool onBike = player->isOnBike();
        if (onBike) {
            speed *=2;
        }

        QPoint direction = player->giveCurrentDirection();
        int nX = player->giveLocation().giveX() + speed * direction.x();
        int nY = player->giveLocation().giveY() + speed * direction.y();
        Interface::Location nLoc;
        nLoc.setXY(nX, nY);
        if (city_->isOnRoad(nLoc)) {
            player->move(nLoc);
            city_->actorMoved(player);
        } else {
            if (direction.x() != 0) {
                nLoc.setXY(nX, nY + 1);
                if (city_->isOnRoad(nLoc)) {
                    player->move(nLoc);
                    city_->actorMoved(player);
                } else {
                    nLoc.setXY(nX, nY - 1);
                    if (city_->isOnRoad(nLoc)) {
                        player->move(nLoc);
                        city_->actorMoved(player);
                    }
                }

            }
            if (direction.y() != 0) {
                nLoc.setXY(nX + 1, nY);
                if (city_->isOnRoad(nLoc)) {
                    player->move(nLoc);
                    city_->actorMoved(player);
                } else {
                    nLoc.setXY(nX - 1, nY);
                    if (city_->isOnRoad(nLoc)) {
                        player->move(nLoc);
                        city_->actorMoved(player);
                    }
                }
            }
        }
        if (!city_->isOnRoad(player->giveLocation())) {
            player->move(city_->findClosestRoad(player->giveLocation()));
            city_->actorMoved(player);
        }

        if (onBike) {
            player->getBike()->move(player->giveLocation());
            city_->actorMoved(player->getBike());
        }
    }
}

void Engine::playerInteract(std::shared_ptr<Player> player)
{
    if (!city_->isGameOver()) {
        if(!player->isInVehicle()) {
            std::shared_ptr<CourseSide::Stop> nearestStop = city_->getNearestStop(player->giveLocation());
            std::shared_ptr<Bicycle> nearestBike = city_->getClosestBike(player->giveLocation());
            double distanceToStop = Interface::Location().calcDistance(nearestStop->getLocation(), player->giveLocation());
            double distanceToBike = Interface::Location().calcDistance(nearestBike->giveLocation(), player->giveLocation());
            if (nearestStop->getLocation().isClose(player->giveLocation())
                    && (distanceToStop <= distanceToBike)) {
                player->setCurrentDirection(QPoint(0,0));
                nearestStop->addPassenger(player);
                player->move(nearestStop->getLocation());
                city_->actorMoved(player);
                player->enterStop(nearestStop);
                player->setWantToGetOff(false);
                player->setWantToGetOn(true);
            }
            else {
                if (nearestBike->giveLocation().isClose(player->giveLocation())) {
                    player->enterVehicle(nearestBike);
                    nearestBike->addPassenger(player);
                }
            }
        }
        else {
            if (player->isInVehicle()) {
                if (player->isOnBike()) {
                    player->getVehicle()->removePassenger(player);
                    city_->actorMoved(player->getBike());
                    player->enterStop(std::shared_ptr<Interface::IStop>(nullptr));
                }
                else {
                    player->setWantToGetOff(true);
                    player->setWantToGetOn(false);
                }
            }
        }
    }
}

void Engine::gameStarted()
{
    Interface::Location startingPoint;

    srand(time(NULL));
    startingPoint.setXY(rand() % 1667 - 500, rand() % 1062 - 337);
    startingPoint = city_->findClosestRoad(startingPoint);
    city_->setStartingPoint(startingPoint);

    Interface::Location goal;
    goal.setXY(rand() % 1667 - 500, rand() % 1062 - 337);
    goal = city_->findClosestRoad(goal);
    city_->setGoal(goal);

    int n = 0;
    while (n < numberOfPlayers_) {
        std::shared_ptr<Player> player = std::make_shared<Player>(Player(startingPoint));
        city_->addActor(player);
        ++n;
    }

    int i = 0;
    while (i < BIKES) {
        Interface::Location nBikeLoc;
        nBikeLoc.setXY(rand() % 1667 - 500, rand() % 1062 - 337);
        while (!city_->isOnRoad(nBikeLoc)) {
            nBikeLoc.setXY(rand() % 1667 - 500, rand() % 1062 - 337);
        }
        std::shared_ptr<Bicycle> bike = std::make_shared<Bicycle>(nBikeLoc);
        city_->addActor(bike);
        ++i;
    }

    logic_->finalizeGameStart();
}

void Engine::star_with_one_player()
{
    numberOfPlayers_=1;
    mainWindow_->show();
    start_window_->close();
}

void Engine::start_with_two_player()
{
    numberOfPlayers_=2;
    mainWindow_->show();
    start_window_->close();
}




