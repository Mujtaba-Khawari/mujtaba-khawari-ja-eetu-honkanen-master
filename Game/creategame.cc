#include "creategame.hh"
#include "interfaces/icity.hh"
#include "city.hh"


std::shared_ptr<Interface::ICity> Interface::createGame()
{
    std::shared_ptr<Interface::ICity> city = std::make_shared<City>(City());
    return city;
}
