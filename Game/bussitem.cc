#include "bussitem.hh"

BussItem::BussItem(int x, int y,unsigned int line) : CourseSide::SimpleActorItem(x, y),b_line (line)
{

}

QRectF BussItem::boundingRect() const
{


        return QRectF(0, 0,  CourseSide::WIDTH, CourseSide::HEIGHT);

}

void BussItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    QRectF bounds = boundingRect();
    QColor color(0,150,200);
    QBrush brush(color);
    painter->setBrush(brush);
    painter->drawEllipse(bounds);
    QFont font=painter->font() ;
    font.setPointSize(8);
    font.setBold(1);
    painter->setFont(font);
    QString strq=QString::number(b_line);
    std::string::size_type s_length =strq.length();
    if(s_length>1){
        painter->drawText(2,12,strq);
    }
    else{
        painter->drawText(4,12,strq);
    }


}

