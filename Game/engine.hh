#ifndef ENGINE_HH
#define ENGINE_HH

#include "mainwindow.hh"
#include "city.hh"
#include "core/logic.hh"
#include "creategame.hh"
#include "player.hh"
#include "actors/stop.hh"
#include "dialog.hh"

#include <QImage>
#include <QObject>
#include <random>

const int BIKES = 15;

class Engine : public QObject
{
    Q_OBJECT

public:
    Engine(MainWindow *mainWindow, CourseSide::Logic *logic, QObject *parent = nullptr);

private slots:
    void movePlayer(std::shared_ptr<Player> player);
    void playerInteract(std::shared_ptr<Player> player);
    void gameStarted();
    void star_with_one_player();
    void start_with_two_player();




private:
    std::shared_ptr<City> city_;
    MainWindow* mainWindow_;
    CourseSide::Logic* logic_;
    std::shared_ptr<Player> player;
    Dialog* start_window_;
    int numberOfPlayers_;

};

#endif // ENGINE_HH
