#include "playeritem.hh"

PlayerItem::PlayerItem(int x, int y, int pnumber) :
  number_(pnumber)
{
    setPos(mapToParent(x - 12, y - 17));
    setZValue(10);
}

QRectF PlayerItem::boundingRect() const
{
    return QRectF(0, 0, W, H);
}

void PlayerItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    QColor color(220, 20, 60);
    QString text = "P1";
    if (number_ > 1) {
        color.setRgb(0, 100, 200);
        text = "P2";
    }
    QBrush brush(color);
    painter->setBrush(brush);
    painter->drawEllipse(QRectF(8, 17, 8, 8));

    QFont font=painter->font() ;
    font.setPointSize(15);
    font.setBold(1);
    painter->setFont(font);
    painter->setPen(color);
    painter->drawText(0, 15, text);
}

void PlayerItem::setCoord(int x, int y)
{
    setX( x - 12 );
    setY( y - 17);
}
