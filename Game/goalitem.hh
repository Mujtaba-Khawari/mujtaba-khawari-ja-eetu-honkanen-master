#ifndef GOALITEM_HH
#define GOALITEM_HH

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPainter>

class GoalItem : public QGraphicsItem
{
public:
    GoalItem(int x, int y);

    QRectF boundingRect() const override;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;
};

#endif // GOALITEM_HH
