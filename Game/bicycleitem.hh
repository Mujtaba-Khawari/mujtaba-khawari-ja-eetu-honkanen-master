#ifndef BICYCLEITEM_HH
#define BICYCLEITEM_HH

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPainter>


const int ICONWIDTH = 20;
const int ICONHEIGHT = 20;

class BicycleItem : public QGraphicsItem
{
public:
    BicycleItem(int x, int y, bool free);

    QRectF  boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;

    void setFree(bool free);
    void setCoord(int x, int y);

private:
    bool free_;
    QImage icon_;
};

#endif // BICYCLEITEM_HH
