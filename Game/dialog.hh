#ifndef DIALOG_HH
#define DIALOG_HH

#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();



signals:
  void on_oneplayerbutton_clicked();
  void on_twoplayerbutton_clicked();

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_HH
