#ifndef HELP_HH
#define HELP_HH

#include <QDialog>
#include <QTextBrowser>

namespace Ui {
class Help;
}

class Help : public QDialog
{
    Q_OBJECT

public:
    explicit Help(QWidget *parent = nullptr);
    ~Help();


private:
    Ui::Help *ui;
};

#endif // HELP_HH
