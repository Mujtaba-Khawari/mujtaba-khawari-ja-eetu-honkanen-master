#include "statistics.hh"

Statistics::Statistics(QObject *parent) : QObject(parent)
{
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &Statistics::timerTick);
}

void Statistics::playerWon(std::shared_ptr<Player> winner)
{
    emit gameOver(winner);
}

void Statistics::gameStarted(std::vector<std::shared_ptr<Player>> players)
{
    gameTime = QTime(0, 0, 0);
    timer->start(1000);
    for (std::shared_ptr<Player> player : players) {
        travelledDistances.insert({player, {player->giveLocation(), 0}});
    };
}

void Statistics::playerMoved(std::shared_ptr<Player> player)
{
    travelledDistances.at(player).second +=
            Interface::Location().calcDistance(travelledDistances.at(player).first, player->giveLocation());
    travelledDistances.at(player).first = player->giveLocation();
}

std::vector<double> Statistics::getTravelledDistances()
{
    std::vector<double> distances = {};

    double dinKm;
    for (auto const& p : travelledDistances) {
        dinKm = static_cast<double>(qRound(p.second.second/10)) / 100;
        distances.push_back(dinKm);
    }

    return distances;
}

void Statistics::gameStop()
{
    timer->stop();
}

void Statistics::timerTick()
{
    gameTime = gameTime.addSecs(1);
    emit gameTimeTick(gameTime, getTravelledDistances());
}
