#ifndef PLAYERITEM_HH
#define PLAYERITEM_HH

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPainter>

const int W = 30;
const int H = 40;

class PlayerItem : public QGraphicsItem
{
public:
    PlayerItem(int x, int y, int pnumber);

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    void setCoord(int x, int y);

private:
    int number_;
};

#endif // PLAYERITEM_HH
