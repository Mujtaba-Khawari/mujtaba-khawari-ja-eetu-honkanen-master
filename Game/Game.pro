TEMPLATE = app
TARGET = NYSSE

QT += core gui widgets network multimedia

CONFIG += c++14

SOURCES += \
    bicycle.cc \
    bicycleitem.cc \
    bussitem.cc \
    city.cc \
    creategame.cc \
    dialog.cc \
    engine.cc \
    goalitem.cc \
    help.cpp \
    main.cc \
    mainwindow.cc \
    player.cc \
    playeritem.cc \
    statistics.cc \
    stopitem.cpp

win32:CONFIG(release, debug|release): LIBS += \
    -L$$OUT_PWD/../Course/CourseLib/release/ -lCourseLib
else:win32:CONFIG(debug, debug|release): LIBS += \
    -L$$OUT_PWD/../Course/CourseLib/debug/ -lCourseLib
else:unix: LIBS += \
    -L$$OUT_PWD/../Course/CourseLib/ -lCourseLib

INCLUDEPATH += \
    $$PWD/../Course/CourseLib

DEPENDPATH += \
    $$PWD/../Course/CourseLib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += \
    $$OUT_PWD/../Course/CourseLib/release/libCourseLib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += \
    $$OUT_PWD/../Course/CourseLib/debug/libCourseLib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += \
    $$OUT_PWD/../Course/CourseLib/release/CourseLib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += \
    $$OUT_PWD/../Course/CourseLib/debug/CourseLib.lib
else:unix: PRE_TARGETDEPS += \
    $$OUT_PWD/../Course/CourseLib/libCourseLib.a

HEADERS += \
    bicycle.hh \
    bicycleitem.hh \
    bussitem.hh \
    city.hh \
    dialog.hh \
    engine.hh \
    goalitem.hh \
    help.hh \
    mainwindow.hh \
    player.hh \
    playeritem.hh \
    statistics.hh \
    stopitem.hh

FORMS += \
    dialog.ui \
    help.ui \
    mainwindow.ui

RESOURCES += \
    resources.qrc

DISTFILES +=
