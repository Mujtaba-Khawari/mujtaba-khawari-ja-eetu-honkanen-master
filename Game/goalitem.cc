#include "goalitem.hh"

GoalItem::GoalItem(int x, int y)
{
    setPos(x - 30,y - 30);
    setZValue(6);
}

QRectF GoalItem::boundingRect() const
{
    return QRectF(0, 0, 60, 60);
}

void GoalItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    static const QPointF points[4] = {
        QPointF(30, 0),
        QPointF(30, 60),
        QPointF(0, 30),
        QPointF(60, 30)
    };

    QColor color(255,0,0);
    QBrush brush(color);
    painter->setBrush(brush);
    painter->drawConvexPolygon(points, 4);
}
