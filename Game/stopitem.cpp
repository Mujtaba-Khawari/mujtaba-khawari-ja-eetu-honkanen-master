#include "stopitem.hh"



Stopitem::Stopitem(int x, int y): x_(x),y_(y)
{
    setPos(mapToParent(x_,y_));
    setZValue(2);
}

void Stopitem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    QRectF bounds = boundingRect();
    QColor color(0,255,0);
    QBrush brush(color);
    painter->setBrush(brush);
    painter->drawRect(bounds);

}

QRectF Stopitem::boundingRect() const
{
    return QRectF(0, 0, WIDTH, HEIGHT);
}

