#ifndef MAINWINDOW_HH
#define MAINWINDOW_HH

#include "stopitem.hh"
#include "graphics/simpleactoritem.hh"
#include "actors/nysse.hh"
#include "bussitem.hh"
#include "player.hh"
#include "playeritem.hh"
#include "bicycle.hh"
#include "bicycleitem.hh"
#include "goalitem.hh"
#include "ui_mainwindow.h"
#include "help.hh"
#include "statistics.hh"

#include <QMainWindow>
#include <QGraphicsScene>
#include <QTimer>
#include <QPushButton>
#include <iostream>
#include <memory>
#include <QVector>
#include <map>
#include <QDebug>

const QPoint NORTH = QPoint(0, 1);
const QPoint SOUTH = QPoint(0, -1);
const QPoint WEST = QPoint(1, 0);
const QPoint EAST = QPoint(-1, 0);


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow() override = default;
    void setPicture(QImage &img);
    void addBus(std::shared_ptr<CourseSide::Nysse> bus);
    void add_stop(std::shared_ptr<CourseSide::Stop> stp);
    void addBicycle(std::shared_ptr<Bicycle> bike);
    void addPlayer(std::shared_ptr<Player> player);
    void addGoal(Interface::Location);
    void removeBus(std::shared_ptr<CourseSide::Nysse> bus);
    void removeStop(std::shared_ptr<CourseSide::Stop> stp);
    void removeBicycle(std::shared_ptr<Bicycle> bike);
    void removePlayer(std::shared_ptr<Player> player);
    void removeGoal(Interface::Location);
    void moveBusOnMap(std::shared_ptr<CourseSide::Nysse> bus);
    void moveBicycleOnMap(std::shared_ptr<Bicycle> bike);
    void movePlayerOnMap(std::shared_ptr<Player> player);
    void keyPressEvent(QKeyEvent* event) override;
    QPoint countGraphicsLocation(Interface::Location loc);
    void showHelpwindow();
    void addStatistics(std::shared_ptr<Statistics> stats);

signals:
    void gameStarted();
    void playerMoved(std::shared_ptr<Player> player);
    void playerToStop(std::shared_ptr<Player> player);


private slots:
    void on_startButton_clicked();
    void onTimerTick();
    void on_pushButton3_clicked();
    void on_pushButton2_clicked();
    void gameOver(std::shared_ptr<Player> winner);
    void showStats(QTime time, std::vector<double> distances);

private:
    Help *new_help_window_ =new Help;
    Ui::MainWindow *ui;
    QGraphicsScene *map;
    QTimer *timer;
    QMap<std::shared_ptr<CourseSide::Nysse>, BussItem*> bussItems_;
    std::vector<std::shared_ptr<Player>> players_;
    QMap<std::shared_ptr<Player>, PlayerItem*> playerItems_;
    QMap<std::shared_ptr<CourseSide::Stop>, Stopitem*> stopItems_;
    QMap<std::shared_ptr<Bicycle>, BicycleItem*> bicycleItems_;
    QGraphicsItem* goal_;
    std::shared_ptr<Statistics> stats_;


    int width_ = 1667; //pxls
    int height_ = 1062;
    int tick_ = 120; //ms
};

#endif // MAINWINDOW_HH
