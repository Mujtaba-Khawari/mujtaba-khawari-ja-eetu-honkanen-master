#include "mainwindow.hh"



#include <QKeyEvent>
#include <QPixmap>

int PADDING = 15;
int MAPWIDTH = 800;
int MAPHEIGHT = 800;

const QString S_START =QString("Game Start");

const QString S_STOP =QString("Game Stop");


MainWindow::MainWindow(QWidget *parent) :
QMainWindow(parent),
ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->gameView->setFixedSize(MAPWIDTH, MAPHEIGHT);
    ui->gameView->move(PADDING, PADDING);

    ui->centralwidget->setFixedSize(MAPWIDTH + 2 * PADDING + ui->travelLabel->width() + 150, MAPHEIGHT + PADDING);
    int vPlacing = PADDING;
    int bHeight = ui->startButton->height();
    ui->startButton->move(MAPWIDTH + 2 * PADDING, vPlacing);
    ui->pushButton2->move(MAPWIDTH + 2 * PADDING, vPlacing += bHeight + PADDING);
    ui->pushButton3->move(MAPWIDTH + 2 * PADDING , vPlacing += bHeight + PADDING);
    ui->clockLabel->move(MAPWIDTH + 2 * PADDING , vPlacing += bHeight + PADDING);
    ui->timerLabel->move(MAPWIDTH + 2 * PADDING, vPlacing += bHeight + PADDING);
    ui->travelLabel->move(MAPWIDTH + 2 * PADDING, vPlacing += bHeight + 2 * PADDING);
    ui->p1dLabel->move(MAPWIDTH + 2 * PADDING, vPlacing += bHeight);
    ui->p2dLabel->move(MAPWIDTH + 2 * PADDING, vPlacing += bHeight);

    ui->startButton->setStyleSheet("background-color: green");
    ui->pushButton3->setText("Close");

    ui->northeast->setFixedSize(150,150);
    ui->northeast->move(MAPWIDTH + 2 * PADDING, vPlacing += 2 * PADDING);
    QPixmap norest_image(":/img/img/north-east.jpg");
    ui->northeast->setPixmap(norest_image.scaled(150,300,Qt::KeepAspectRatio));


    ui->legend->setFixedSize(220,370);
    ui->legend->move(MAPWIDTH + 2 * PADDING, vPlacing += ui->northeast->height() + PADDING);
    QPixmap legend_image(":/img/img/legend1.png");
    ui->legend->setPixmap(legend_image.scaled(220,370,Qt::KeepAspectRatio));




    map = new QGraphicsScene(this);
    ui->gameView->setScene(map);
    map->setSceneRect(0,0,width_,height_);
    resize(minimumSizeHint());
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, map, &QGraphicsScene::advance);
    connect(timer, &QTimer::timeout, this, &MainWindow::onTimerTick);
    timer->start(tick_);
}

void MainWindow::setPicture(QImage &img)
{
    map->setBackgroundBrush(img);
}

void MainWindow::addBus(std::shared_ptr<CourseSide::Nysse> bus)
{
    QPoint point = countGraphicsLocation(bus->giveLocation());
    unsigned int bus_line=bus->getLine();
    BussItem* nBusItem = new BussItem(point.x(), point.y(),bus_line);
    bussItems_.insert(bus, nBusItem);

    map->addItem(nBusItem);
}

void MainWindow::add_stop(std::shared_ptr<CourseSide::Stop> stp)
{
    QPoint point = countGraphicsLocation(stp->getLocation());
    Stopitem* new_stop=new Stopitem(point.x(),point.y());
    stopItems_.insert(stp, new_stop);
    map->addItem(new_stop);
}

void MainWindow::addBicycle(std::shared_ptr<Bicycle> bike)
{
    QPoint point = countGraphicsLocation(bike->giveLocation());
    BicycleItem* nBikeItem = new BicycleItem(point.x(), point.y(), bike->isFree());
    bicycleItems_.insert(bike, nBikeItem);
    map->addItem(nBikeItem);
}

void MainWindow::addPlayer(std::shared_ptr<Player> player)
{
    players_.push_back(player);
    QPoint point = countGraphicsLocation(player->giveLocation());
    int pnumber = static_cast<int>(players_.size());
    PlayerItem* new_player = new PlayerItem(point.x(), point.y(), pnumber);
    playerItems_.insert(player, new_player);
    map->addItem(new_player);
}

void MainWindow::addGoal(Interface::Location loc)
{
    QPoint point = countGraphicsLocation(loc);
    goal_ = new GoalItem(point.x(), point.y());
    map->addItem(goal_);
    ui->gameView->ensureVisible(goal_, 200, 200);
}

void MainWindow::removeBus(std::shared_ptr<CourseSide::Nysse> bus)
{
    bussItems_.remove(bus);
}

void MainWindow::removeStop(std::shared_ptr<CourseSide::Stop> stp)
{
    stopItems_.remove(stp);
}

void MainWindow::removeBicycle(std::shared_ptr<Bicycle> bike)
{
    bicycleItems_.remove(bike);
}

void MainWindow::removePlayer(std::shared_ptr<Player> player)
{
    for (auto it = players_.begin(); it != players_.end(); it++) {
        if (it->get() == player.get()) {
            players_.erase(it);
            break;
        }
    }
    playerItems_.remove(player);
}

void MainWindow::removeGoal(Interface::Location)
{
    goal_ = nullptr;
}

void MainWindow::moveBusOnMap(std::shared_ptr<CourseSide::Nysse> bus)
{
    QPoint point = countGraphicsLocation(bus->giveLocation());
    bussItems_[bus]->setCoord(point.x(), point.y());
}

void MainWindow::moveBicycleOnMap(std::shared_ptr<Bicycle> bike)
{
    QPoint point = countGraphicsLocation(bike->giveLocation());
    bicycleItems_[bike]->setCoord(point.x(), point.y());
    bicycleItems_[bike]->setFree(bike->isFree());
}

void MainWindow::movePlayerOnMap(std::shared_ptr<Player> player)
{
    QPoint point = countGraphicsLocation(player->giveLocation());
    playerItems_[player]->setCoord(point.x(), point.y());

    ui->gameView->ensureVisible(playerItems_[player]);
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if (players_.size() > 0) {
        if(event->key() == Qt::Key_W) {
             players_.at(0)->setCurrentDirection(NORTH);
        }
        if(event->key() == Qt::Key_S) {
             players_.at(0)->setCurrentDirection(SOUTH);
        }
        if(event->key() == Qt::Key_D) {
             players_.at(0)->setCurrentDirection(WEST);
        }
        if(event->key() == Qt::Key_A) {
             players_.at(0)->setCurrentDirection(EAST);
        }
        if(event->key() == Qt::Key_Space) {
            emit playerToStop(players_.at(0));
        }
    }
    if (players_.size() == 2) {
        if(event->key() == Qt::Key_O) {
             players_.at(1)->setCurrentDirection(NORTH);
        }
        if(event->key() == Qt::Key_L) {
             players_.at(1)->setCurrentDirection(SOUTH);
        }
        if(event->key() == Qt::Key_Odiaeresis) {
             players_.at(1)->setCurrentDirection(WEST);
        }
        if(event->key() == Qt::Key_K) {
             players_.at(1)->setCurrentDirection(EAST);
        }
        if(event->key() == Qt::Key_Shift) {
            emit playerToStop(players_.at(1));
        }
    }
}

QPoint MainWindow::countGraphicsLocation(Interface::Location loc)
{
    int px = loc.giveX() + 500;
    int py = height_ - (loc.giveY() + 337);
    return QPoint(px,py);
}

void MainWindow::showHelpwindow()
{

    new_help_window_->show();
    new_help_window_->setWindowTitle("NYSSE USER GUIDE");
}

void MainWindow::addStatistics(std::shared_ptr<Statistics> stats)
{
    stats_ = stats;
    connect(stats_.get(), &Statistics::gameOver, this, &MainWindow::gameOver);
    connect(stats_.get(), &Statistics::gameTimeTick, this, &MainWindow::showStats);
}

void MainWindow::on_startButton_clicked()
{
        qDebug() << "Start clicked";

        emit gameStarted();

        ui->startButton->setStyleSheet("background-color: red");
        ui->startButton->setEnabled(0);
        ui->pushButton3->setStyleSheet("background-color: green");
        ui->pushButton3->setText("Finish Game");
}

void MainWindow::onTimerTick()
{
    for (std::shared_ptr<Player> player : players_) {
        if(player->giveCurrentDirection() != QPoint(0,0)) {
            emit playerMoved(player);
        }
    }

}
void MainWindow::on_pushButton3_clicked()
{

    this->close();
    new_help_window_->close();

}
void MainWindow::on_pushButton2_clicked()
{
    showHelpwindow();
}

void MainWindow::gameOver(std::shared_ptr<Player> winner)

{
    std::vector<std::shared_ptr<Player>>::size_type size=players_.size();
    int new_size =static_cast<int>(size);

    if (new_size==1){

        ui->gameOverLabel->setText("YOU WON!");

    }
    else{
        if(players_.at(0)==winner){
            ui->gameOverLabel->setText("RED PLAYER WON!");

        }
        else{
            ui->gameOverLabel->setText("BLUE PLAYER WON!");
        }

        }
    ui->gameOverLabel->move(PADDING + (MAPWIDTH - ui->gameOverLabel->width())/2,
                            PADDING + (MAPHEIGHT - ui->gameOverLabel->height())/2);
}


void MainWindow::showStats(QTime time, std::vector<double> distances)
{
    ui->timerLabel->setText(time.toString("m:ss"));

    QString text = "P1: " + QString::number(distances.at(0));
    ui->p1dLabel->setText(text);

    if (static_cast<int>(distances.size()) > 1) {
        text = "P2: " + QString::number(distances.at(1));
        ui->p2dLabel->setText(text);
    }
}
