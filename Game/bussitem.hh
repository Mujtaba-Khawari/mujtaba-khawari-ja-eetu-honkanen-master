#ifndef BUSSITEM_HH
#define BUSSITEM_HH

#include <graphics/simpleactoritem.hh>

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPainter>
#include <string>

class BussItem : public CourseSide::SimpleActorItem
{
public:
    BussItem(int x, int y, unsigned int line );
    QRectF  boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);


protected:
    unsigned int b_line;

};

#endif // BUSSITEM_HH
