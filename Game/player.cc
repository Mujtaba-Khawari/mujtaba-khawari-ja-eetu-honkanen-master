#include "player.hh"

Player::Player(Interface::Location loc) : CourseSide::Passenger(std::weak_ptr< Interface::IStop>()),
  loc_(loc),
  currentDirection_(QPoint(0,0)),
  wantToGetOff_(false),
  wantToGetOn_(false)
{

}

Interface::Location Player::giveLocation() const
{
    return loc_;
}

void Player::move(Interface::Location loc)
{
    loc_ =loc;
}

bool Player::wantToEnterVehicle(std::weak_ptr<Interface::IVehicle> vehicle) const
{
    Q_UNUSED(vehicle);
    return wantToGetOn_;
}

bool Player::wantToEnterNysse(std::weak_ptr<CourseSide::Nysse> nysse) const
{
    Q_UNUSED(nysse);
    return wantToGetOn_;
}

bool Player::wantToEnterStop(std::weak_ptr<Interface::IStop> stop) const
{
    Q_UNUSED(stop);
    return wantToGetOff_;
}


void Player::setCurrentDirection(QPoint direction)
{
    currentDirection_ = direction;
}

QPoint Player::giveCurrentDirection()
{
    return currentDirection_;
}

void Player::setWantToGetOff(bool wantToGetOff)
{
    wantToGetOff_ = wantToGetOff;
}

void Player::setWantToGetOn(bool wantToGetOn)
{
    wantToGetOn_ = wantToGetOn;
}

std::shared_ptr<Bicycle> Player::getBike()
{
    std::shared_ptr<Bicycle> bike = std::dynamic_pointer_cast<Bicycle>(getVehicle());
    return bike;
}

bool Player::isOnBike()
{
    if(getBike() != 0) {
        return true;
    }
    return false;
}
