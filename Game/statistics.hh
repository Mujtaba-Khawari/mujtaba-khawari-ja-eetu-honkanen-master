#ifndef STATISTICS_H
#define STATISTICS_H

#include "player.hh"
#include "core/location.hh"

#include <QObject>
#include <QTimer>
#include <QTime>

class Statistics : public QObject
{
    Q_OBJECT
public:
    explicit Statistics(QObject *parent = nullptr);

    void playerWon(std::shared_ptr<Player> winner);
    void gameStarted(std::vector<std::shared_ptr<Player>> players);
    void playerMoved(std::shared_ptr<Player> player);
    std::vector<double> getTravelledDistances();
    void gameStop();

signals:

    void gameOver(std::shared_ptr<Player> winner);
    void gameTimeTick(QTime spentTime, std::vector<double> travelledDistances);

private slots:

    void timerTick();

private:
    QTimer* timer;
    QTime gameTime;
    std::map<std::shared_ptr<Player>, std::pair<Interface::Location, double>> travelledDistances;
};

#endif // STATISTICS_H
