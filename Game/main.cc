#include "creategame.hh"
#include <QApplication>
#include <QFile>

#include "engine.hh"
#include "mainwindow.hh"
#include "interfaces/icity.hh"
#include "creategame.hh"
#include "core/logic.hh"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Q_INIT_RESOURCE(offlinedata);
    QImage image(":/kartta_pieni_500x500.png");

    MainWindow w;
    w.setWindowTitle("Nysse Game");
    CourseSide::Logic l;
    Engine e(&w, &l);


    return a.exec();
}
