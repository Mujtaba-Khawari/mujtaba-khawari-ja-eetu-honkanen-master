#ifndef STOPITEM_HH
#define STOPITEM_HH

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPainter>
const int WIDTH = 8;
const int HEIGHT = 8;
class Stopitem :public QGraphicsItem
{
public:
    explicit Stopitem(int x ,int y);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option2, QWidget *widget2);
    QRectF boundingRect() const;
private:
    int x_;
    int y_;

};

#endif // STOPITEM_HH
