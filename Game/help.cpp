#include "help.hh"
#include "ui_help.h"
#include <QPixmap>

Help::Help(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Help)
{
    ui->setupUi(this);
    this->setFixedSize(1250,600);
    ui->keyboard->setFixedSize(1300,600);
    QPixmap help_image(":/img/img/keyboard.png");
    ui->keyboard->setPixmap(help_image.scaled(1300,600,Qt::KeepAspectRatio));


}

Help::~Help()
{
    delete ui;
}


