#ifndef PLAYER_HH
#define PLAYER_HH

#include "actors/passenger.hh"
#include "actors/stop.hh"
#include "bicycle.hh"


class Player : public CourseSide::Passenger
{
public:
    Player(Interface::Location loc);

    Interface::Location giveLocation() const override;
    void move(Interface::Location loc) override;
    bool wantToEnterVehicle(std::weak_ptr<Interface::IVehicle> vehicle) const override;
    bool wantToEnterNysse(std::weak_ptr<CourseSide::Nysse> nysse) const override;
    bool wantToEnterStop(std::weak_ptr<Interface::IStop> stop) const override;

    void setCurrentDirection(QPoint direction);
    QPoint giveCurrentDirection();
    void setWantToGetOff(bool wantToGetOff);
    void setWantToGetOn(bool wantToGetOn);
    std::shared_ptr<Bicycle> getBike();
    bool isOnBike();

private:
    Interface::Location loc_;
    QPoint currentDirection_;
    bool wantToGetOff_;
    bool wantToGetOn_;
};

#endif // PLAYER_HH
