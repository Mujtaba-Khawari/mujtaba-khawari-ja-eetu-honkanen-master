#include "bicycleitem.hh"

BicycleItem::BicycleItem(int x, int y, bool free) :
  free_(free),
  icon_(":/img/img/bicycleIcon.png")
{
    icon_ = icon_.scaled(ICONWIDTH, ICONHEIGHT, Qt::KeepAspectRatio);
    setPos(mapToParent(x -5, y -5));
    setZValue(5);
}

QRectF BicycleItem::boundingRect() const
{
    return QRectF(0, 0,  ICONWIDTH, ICONHEIGHT);
}

void BicycleItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    if (free_) {
        painter->setBrush(QColor(255, 255, 10));
        painter->drawEllipse(boundingRect());
    }
    painter->drawImage(boundingRect(), icon_);
}

void BicycleItem::setFree(bool free)
{
    free_ = free;
}

void BicycleItem::setCoord(int x, int y)
{
    setX( x - 5 );
    setY( y - 5 );
}
