#ifndef CITY_HH
#define CITY_HH

#include "interfaces/ivehicle.hh"
#include "interfaces/icity.hh"
#include "interfaces/ipassenger.hh"
#include "interfaces/iactor.hh"
#include "interfaces/istop.hh"
#include "interfaces/istatistics.hh"
#include "actors/nysse.hh"
#include "actors/passenger.hh"
#include "actors/stop.hh"
#include "graphics/simplemainwindow.hh"
#include "mainwindow.hh"
#include "errors/gameerror.hh"
#include "player.hh"
#include "bicycle.hh"
#include "statistics.hh"

#include <memory>
#include <vector>
#include <map>
#include <iterator>
#include <algorithm>
#include <math.h>
#include <QImage>
#include <QFile>
#include <QTime>
#include <QString>
const QString DEFAULT_ROAD_FILE = ":/textfile/textfile/tiet.txt";

class City : public Interface::ICity
{
public:
    City();

    //ICity interface
    void setBackground(QImage &basicbackground, QImage &bigbackground) override;
    void setClock(QTime clock) override;
    void addStop(std::shared_ptr<Interface::IStop> stop) override;
    void startGame() override;
    void addActor(std::shared_ptr<Interface::IActor> newactor) override;
    void removeActor(std::shared_ptr<Interface::IActor> actor) override;
    void actorRemoved(std::shared_ptr<Interface::IActor> actor) override;
    void actorMoved(std::shared_ptr<Interface::IActor> actor) override;
    bool isGameOver() const override;
    bool findActor(std::shared_ptr<Interface::IActor> actor) const override;
    std::vector<std::shared_ptr<Interface::IActor>> getNearbyActors(Interface::Location loc) const override;

    void stopGame();
    void setMainWindow(MainWindow* mainWindow);
    std::shared_ptr<CourseSide::Stop> getNearestStop(Interface::Location loc);
    bool isOnRoad(Interface::Location loc);
    Interface::Location findClosestRoad(Interface::Location loc);
    std::shared_ptr<Bicycle> getClosestBike(Interface::Location loc);
    bool isPlayerWon(std::shared_ptr<Player> player);
    Interface::Location getStartingPoint() const;
    void setStartingPoint(const Interface::Location &startingPoint);
    Interface::Location getGoal() const;
    void setGoal(const Interface::Location &goal);
    void setStatistics();

private:   
    bool gameOn_;
    MainWindow* mainWindow_;
    QTime clock_;
    std::vector<std::shared_ptr<Interface::IActor>> actors_;
    std::vector<std::shared_ptr<CourseSide::Nysse>> busses_;
    std::vector<std::shared_ptr<CourseSide::Passenger>> passengers_;
    std::vector<std::shared_ptr<CourseSide::Stop>> stops_;
    std::vector<std::shared_ptr<Bicycle>> bikes_;
    std::vector<std::shared_ptr<Player>> players_;
    QImage roadMap_;
    Interface::Location startingPoint_;
    Interface::Location goal_;
    std::shared_ptr<Statistics> stats_;
};

#endif // CITY_HH
