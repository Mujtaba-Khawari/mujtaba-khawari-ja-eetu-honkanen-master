#include "city.hh"


City::City() :
    gameOn_(false),
    mainWindow_(nullptr),
    busses_({}),
    passengers_({}),
    stops_({}),
    bikes_({}),
    players_({}),
    roadMap_(":/img/img/roadMap.png"),
    startingPoint_(),
    goal_(),
    stats_()
{
    roadMap_ = roadMap_.convertToFormat(QImage::Format_Mono);

}

void City::setBackground(QImage &basicbackground, QImage &bigbackground)
{
    Q_UNUSED(bigbackground);
    mainWindow_->setPicture(basicbackground);
}

void City::setClock(QTime clock)
{
    clock_ = clock;
}

void City::addStop(std::shared_ptr<Interface::IStop> stop)
{
    std::shared_ptr<CourseSide::Stop> stp1 = std::dynamic_pointer_cast<CourseSide::Stop>(stop);
    if (stp1 != 0) {
        stops_.push_back(stp1);
        mainWindow_->add_stop(stp1);
    }

}

void City::startGame()
{
    gameOn_ = true;
    stats_->gameStarted(players_);
}

void City::addActor(std::shared_ptr<Interface::IActor> newactor)
{
    actors_.push_back(newactor);

    std::shared_ptr<CourseSide::Nysse> nysse = std::dynamic_pointer_cast<CourseSide::Nysse>(newactor);

    if (nysse != 0) {
        busses_.push_back(nysse);
        mainWindow_->addBus(nysse);
    }

    else {
        std::shared_ptr<Player> player = std::dynamic_pointer_cast<Player>(newactor);

        if (player != 0) {
            players_.push_back(player);
            mainWindow_->addPlayer(player);
        }

        else {
            std::shared_ptr<CourseSide::Passenger> passenger = std::dynamic_pointer_cast<CourseSide::Passenger>(newactor);

            if (passenger != 0) {
                passengers_.push_back(passenger);
            }

            else {
                std::shared_ptr<Bicycle> bike = std::dynamic_pointer_cast<Bicycle>(newactor);

                if (bike != 0) {
                    bikes_.push_back(bike);
                    mainWindow_->addBicycle(bike);
                }
            }
        }
    }
}

void City::removeActor(std::shared_ptr<Interface::IActor> actor)
{
    if (findActor(actor)) {
        std::shared_ptr<CourseSide::Nysse> nysse = std::dynamic_pointer_cast<CourseSide::Nysse>(actor);

        if (nysse != 0) {
            for (auto it = busses_.begin(); it != busses_.end(); it++) {
                if (it->get() == nysse.get()) {
                    busses_.erase(it);
                    mainWindow_->removeBus(nysse);
                    break;
                }
            }
        }

        else {
            std::shared_ptr<Player> player = std::dynamic_pointer_cast<Player>(actor);

            if (player != 0) {
                for (auto it = players_.begin(); it != players_.end(); it++) {
                    if (it->get() == player.get()) {
                        players_.erase(it);
                        mainWindow_->removePlayer(player);
                        break;
                    }
                }
            }

            else {
                std::shared_ptr<CourseSide::Passenger> passenger = std::dynamic_pointer_cast<CourseSide::Passenger>(actor);

                if (passenger != 0) {
                    for (auto it = passengers_.begin(); it != passengers_.end(); it++) {
                        if (it->get() == passenger.get()) {
                            passengers_.erase(it);
                            break;
                        }
                    }
                }

                else {
                    std::shared_ptr<Bicycle> bike = std::dynamic_pointer_cast<Bicycle>(actor);

                    if (bike != 0) {
                        for (auto it = bikes_.begin(); it != bikes_.end(); it++) {
                            if (it->get() == bike.get()) {
                                bikes_.erase(it);
                                mainWindow_->removeBicycle(bike);
                                break;
                            }
                        }
                    }
                }
            }
        }
        actors_.erase(std::find(actors_.begin(), actors_.end(), actor));
        actorRemoved(actor);
    }
    else {
        throw Interface::GameError();
    }
}

void City::actorRemoved(std::shared_ptr<Interface::IActor> actor)
{
    actor->remove();
}

void City::actorMoved(std::shared_ptr<Interface::IActor> actor)
{
    std::shared_ptr<CourseSide::Nysse> nysse = std::dynamic_pointer_cast<CourseSide::Nysse>(actor);

    if (nysse != 0) {
        mainWindow_->moveBusOnMap(nysse);
    }

    else {
        std::shared_ptr<Player> player = std::dynamic_pointer_cast<Player>(actor);
        if (player != 0) {
            mainWindow_->movePlayerOnMap(player);
            stats_->playerMoved(player);
            if (player->getStop() != nullptr) {
                std::dynamic_pointer_cast<CourseSide::Stop>(player->getStop())->removePassenger(player);
            }
            if (player->giveLocation().isClose(goal_)) {
                stats_->playerWon(player);
                stopGame();

            }
        }

        else {
            std::shared_ptr<Bicycle> bike = std::dynamic_pointer_cast<Bicycle>(actor);
            if (bike != 0) {
                mainWindow_->moveBicycleOnMap(bike);
            }
        }
    }

}

bool City::isGameOver() const
{
    return !gameOn_;
}

bool City::findActor(std::shared_ptr<Interface::IActor> actor) const
{
    if (std::find(actors_.begin(), actors_.end(), actor) != actors_.end()) {
        return true;
    }
    else {
        return false;
    }
}

std::vector<std::shared_ptr<Interface::IActor> > City::getNearbyActors(Interface::Location loc) const
{
    std::vector<std::shared_ptr<Interface::IActor> > nearbyActors = {};

    for (std::shared_ptr<Interface::IActor> actor : actors_) {
        if (loc.isClose(actor->giveLocation())) {
            nearbyActors.push_back(actor);
        }
    }
    return nearbyActors;
}

void City::stopGame()
{
    gameOn_ = false;
    stats_->gameStop();
}



void City::setMainWindow(MainWindow *mainWindow)
{
    mainWindow_ = mainWindow;
}

std::shared_ptr<CourseSide::Stop> City::getNearestStop(Interface::Location loc)
{
    std::shared_ptr<CourseSide::Stop> nearest;
    for(std::shared_ptr<CourseSide::Stop> stop : stops_) {
        if (nearest == nullptr || loc.calcDistance(stop->getLocation(), loc) < loc.calcDistance(nearest->getLocation(), loc)) {
            nearest = stop;
        }
    }

    return nearest;
}

bool City::isOnRoad(Interface::Location loc)
{
    int px = loc.giveX() + 500;
    int py = 1062 - (loc.giveY() + 333);
    if (px > 0 &&  px < roadMap_.width() && py > 0 && py < roadMap_.height()) {
        if (roadMap_.pixelColor(px, py).black() > 1) {
            return true;
        }
    }
    return false;
}

Interface::Location City::findClosestRoad(Interface::Location loc)
{
    Interface::Location closestRoad = loc;
    int dy = 0;
    int distance = 0;
    while (distance < 1000) {
        for (int dx = -distance; dx < distance; ++dx) {
             dy = sqrt(distance*distance - dx*dx);
             closestRoad.setXY(loc.giveX()+dx, loc.giveY()+dy);
             if (isOnRoad(closestRoad)) {
                 return closestRoad;
             }
             closestRoad.setXY(loc.giveX()+dx, loc.giveY()-dy);
             if (isOnRoad(closestRoad)) {
                 return closestRoad;
             }
        }
        ++distance;
    }
}

std::shared_ptr<Bicycle> City::getClosestBike(Interface::Location loc)
{
    std::shared_ptr<Bicycle> closest = bikes_.at(0);
    for (std::shared_ptr<Bicycle> bike : bikes_) {
        if (loc.calcDistance(bike->giveLocation(), loc) < loc.calcDistance(closest->giveLocation(), loc)) {
            closest = bike;
        }
    }
    return closest;
}

Interface::Location City::getStartingPoint() const
{
    return startingPoint_;
}

void City::setStartingPoint(const Interface::Location &startingPoint)
{
    startingPoint_ = startingPoint;
}

Interface::Location City::getGoal() const
{
    return goal_;
}

void City::setGoal(const Interface::Location &goal)
{
    goal_ = goal;
    mainWindow_->addGoal(goal_);
}

void City::setStatistics()
{
    stats_ = std::make_shared<Statistics>();
    mainWindow_->addStatistics(stats_);
}



