#include "statistics.hh"

#include <QtTest>

class gameTest : public QObject
{
    Q_OBJECT

public:
    gameTest();
    ~gameTest();

private Q_SLOTS:
    void statisticsTest();

};

gameTest::gameTest()
{

}

gameTest::~gameTest()
{

}

void gameTest::statisticsTest()
{
    std::shared_ptr<Player> p1 = std::make_shared<Player>(Interface::Location());
    std::shared_ptr<Player> p2 = std::make_shared<Player>(Interface::Location());

    std::vector<std::shared_ptr<Player>> pVec = {p1, p2};

    Statistics stats;
    stats.gameStarted(pVec);

    // Players haven't moved yet
    std::vector<double> dVec = {0, 0};
    QCOMPARE(stats.getTravelledDistances(), dVec);

    Interface::Location locP1_2(6700000 + 15932, 3500000);
    Interface::Location locP2_2(6700000, 3500000 + 100000);

    p1->move(locP1_2);
    p2->move(locP2_2);

    dVec = {100000, 15930};

    stats.playerMoved(p1);
    stats.playerMoved(p2);

    //Players have moved
    QCOMPARE(qRound(stats.getTravelledDistances().at(0)*1000), dVec.at(0));
    QCOMPARE(qRound(stats.getTravelledDistances().at(1)*1000), dVec.at(1));
}

QTEST_APPLESS_MAIN(gameTest)

#include "tst_gametest.moc"
